=begin
Crie uma função, que dado um array de inteiros, retorne um array 
com apenas os divisíveis por 3.
ex: entrada = [3,6,7,8]
    saída =[3,6]
=end

def divisiveis_3(lista)
    array = []
    for number in lista
        if( (number % 3) == 0)
            array.push(number)
        end
    end
    print array
end

entrada = [3,6,7,8]
divisiveis_3(entrada)