=begin
Crie uma função, que dado um array de inteiros, 
faça um cast para string e retorne um array com os valores em string
ex: entrada = [25,35,45]
    saída =["25","35","45"]
=end

def strings(lista)
    array = []
    a = 0
    for valor in lista
        a = valor.to_s
        array.push(a)
    end
    print array
end    

entrada = [25,35,45]
strings(entrada)
