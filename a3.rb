=begin
Crie uma função, que dado um hash como paramêtro, e retorne um array 
com todos os valores que estão no hash elevados ao quadrado.
ex: entrada = {:chave => 5,:chave2 => 30, :chave3 => 20}
    saída =[25,900,400]
=end

def exponenciação(dict)
    array = []
    a = 0

    dict.each do |chave, valor|
        
        a = valor ** 2
        array.push(a)    
    
    end
    
    print array
end

entrada = {:chave => 5,:chave2 => 30, :chave3 => 20}
exponenciação(entrada)
