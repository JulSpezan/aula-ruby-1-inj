=begin
Crie uma função que dado um array de arrays, imprima na tela a soma e a
multiplicação de todos os valores.
ex: [[2,5,7],[3,2,4,10],[1,2,3]] = soma = 39, multiplicação = 100800
=end

def soma_e_multiplica(lista)
    soma = 0
    multiplicação = 1

    for valor in lista 
        
        for num in valor
            soma += num
            multiplicação *= num
        end
    end
    puts soma
    puts multiplicação
end

array = [[2,5,7],[3,2,4,10],[1,2,3]]
soma_e_multiplica(array)